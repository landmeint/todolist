<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodolistController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'App\Http\Controllers\TodolistController@index');
// Route::post('save', 'App\Http\Controllers\TodolistController@store');
// Route::get('edit/{id}', 'App\Http\Controllers\TodolistController@edit');
// Route::get('update/{id}', 'App\Http\Controllers\TodolistController@update');
// Route::get('delete/{id}', 'App\Http\Controllers\TodolistController@destroy');

// Route::controller(App\Http\Controllers\UserController::class)->group(function() {
//     Route::post('/logi', 'login');
//     Route::get('/login', 'loginPage');
//     Route::post('/logo', 'logout');
// });

Route::get('/cast',[CastController::class,'index']);
Route::get('/cast/create',[CastController::class,'create']);
Route::post('/cast',[CastController::class,'store']);
Route::get('/cast/{cast_id}',[CastController::class,'show']);
Route::get('/cast/{cast_id}/edit',[CastController::class,'edit']);
Route::put('/cast/{cast_id}',[CastController::class,'update']);
Route::delete('/cast/{cast_id}',[CastController::class,'destroy']);