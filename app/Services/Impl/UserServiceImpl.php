<?php

namespace App\Services\Impl;

use App\Services\UserService;

class UserServiceImpl implements UserService {
    private array $users = [
        "thunderlab" => "wonderland"
    ];
    
    function login(string $user, string $pass):bool {
        if(!isset($this->users[$user])) {
            return false;
        }

        $correctPass = $this->users[$user];
        return $pass == $correctPass;
    }
}