<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function loginPage() {
        return view('login.login');
    }

    public function login(Request $request) {
        $user = $request->input('user');
        $pass = $request->input('pass');

        if($this->userService->login($user,$pass)) {
            $request->session()->put("user",$user);
            return redirect("/");
        }

        return view('login.login');
    }

    public function logout(Request $request) {
        $request->session()->forget("user");
        return redirect("/login");
    }
}
