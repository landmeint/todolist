<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Todolist</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
    <h1 style="text-align: center">TODOLIST</h1>
    <div class="container">
        <div class="card mx-auto" style="width: 40rem;">
            <div class="card-header">
                Edit Tugas
            </div>
            <div class="card-body">
                <form class="row g-3" action="../update/{{ $todo->id }}">
                    <div class="col-8">
                      <label for="editTodo" class="visually-hidden">Edit Tugas</label>
                      <input type="text" name="note" class="form-control" id="editTodo" value="{{ $todo->note }}" required>
                    </div>
                    <div class="col-auto">
                      <button type="submit" class="btn btn-primary mb-3">Edit</button>
                    </div>
                  </form>
            </div>
        </div>
    </div>





    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
</body>
</html>