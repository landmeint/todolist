<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cast</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
    <h1 style="text-align: center">CAST PAGE</h1>
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#tambahModal">
                    Tambah Cast
                </button>
            </div>
            <div class="card-body">
              <table class="table">
                <tr>
                    <th>Daftar Cast</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th>Tanggal</th>
                    <th>Action</th>
                </tr>
                @foreach ($cast as $td)
                <tr>
                    <td>{{ $td->nama }}</td>
                    <td>{{ $td->umur }}</td>
                    <td>{{ $td->bio }}</td>
                    <td>{{date('D d F, Y H:i',strtotime($td->created_at)) }}</td>
                    <td>
                        <a href="cast/{{ $td->id }}" class="btn btn-danger">Tampil</a>
                        <a href="cast/{{ $td->id }}/edit" class="btn btn-danger">Edit</a>
                        <a href="cast/{{ $td->id }}" class="btn btn-danger">Hapus</a> 
                    </td>
                </tr>
                @endforeach
              </table> 
            </div>
        </div>
    </div>

    <!-- Modal Tambah -->
    <div class="modal fade" id="tambahModal" tabindex="-1" aria-labelledby="tambahModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h1 class="modal-title fs-5" id="tambahModalLabel">Tambah Tugas</h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            <form class="row g-3" action="save" method="POST">
                @csrf
                <div class="col-9">
                  <label for="note" class="visually-hidden">Tambah Tugas</label>
                  <input type="text" class="form-control" name="note" id="note" placeholder="Tulis Tugas Hari Ini" required>
                </div>
                <div class="col-auto">
                  <button type="submit" class="btn btn-primary mb-3">Simpan</button>
                </div>
            </form>
            </div>
            
        </div>
        </div>
    </div>




    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
    <script>
        
    </script>
</body>
</html>